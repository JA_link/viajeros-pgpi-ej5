== ﻿Hospedajes

//Establecer un apartado por cada tipo de hospedaje en cada ciudad
// Ordenada por orden alfabético en cada apartado
//Precio entre ()

=== Alicante

==== Hoteles
	* Hotel eurostar Velum (47€/persona)


=== Barcelona

==== Hoteles
	* H10 Cubik (96€/persona)


=== Ciudad Real

==== Hoteles
	* Hotel NH Ciudad Real (56€/persona)


=== Córdoba

===== Hoteles
	* Hotel Córdoba Center (80€/persona)


=== Granada

===== Hoteles
	* Hotel EL Granado 2* (31€/noche)


=== Madrid

==== Hoteles
	* Hotel NH Madrid Atocha (118€/persona)


=== Murcia

==== Hoteles
	* Hotel Nelva (61€/persona)


=== Tarragona

==== Hoteles
	* AC Hotel Tarragona (80€/persona)


=== Toledo

==== Hoteles
	* Hotel Real de Toledo (52€/persona)


=== Valencia

==== Hoteles
	* Hotel Zenit Valencia (94€/persona)

=== Zaragoza

==== Hoteles
	* Hotel NH Collection Gran Hotel de Zaragoza (81€/persona)
